# License

This repository contains a combination of code and content under various licenses.

## Source files in the repository

Unless explicitly stated, all files in this repository are licensed under the terms of the [GNU General Public License v3.0 (GPL-3.0)](https://www.gnu.org/licenses/gpl-3.0).

* The LaTeX files included in [exports/shared_code/latex/](./exports/shared_code/latex/) are licensed under [LPPL (LaTeX Project Public License)](https://www.latex-project.org/lppl/) or (at your opinion) any later version.
* The images included in [exports/shared_code/images/gregor/](./exports/shared_code/images/) are licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) or (at your opinion) any later version.
* The images included in [exports/shared_code/images/mpv/](./exports/shared_code/images/) do not have a license. The [MPV](https://www.uni-augsburg.de/de/fakultaet/mntf/mpv/) grants permission for the exclusive use of these images by the [Mathezirkel Augsburg](https://www.mathezirkel-augsburg.de). Any unauthorized use of the files is strictly prohibited.

## Compiled files

Any compiled files derived from them are licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) or (at your opinion) any later version.
