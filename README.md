# Mathezirkel App Exports

## Custom Presets

The `*.graphql` may include the placeholders `%%ZIRKEL_UUID%%` and `%%EVENT_UUID%%`.

## Generate the generated.json file

```shell
docker compose up
```

## PKL

### Installation

[How to run pkl](https://pkl-lang.org/main/current/pkl-cli/index.html)

Here packaged to docker:

```cmd
docker build -t pkl:latest -f docker/pkl/Dockerfile .
```

Usage:

```cmd
docker run -v "$(pwd)":/tmp pkl:latest --version
docker run -v "$(pwd)":/tmp pkl:latest
```

You could set an alias in the `.bashrc`

```cmd
echo "alias pkl='docker run -v \"\$(pwd)\":/tmp pkl:latest'" >> ~/.bashrc
```

### Syntax-Highlighting: Install the VS-Code extension

(Language Server not yet supported)

[How to install extension](https://pkl-lang.org/vscode/current/installation.html)
