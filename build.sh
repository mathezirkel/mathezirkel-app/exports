#!/bin/bash

(find . -type f -name "*.base64" -print0 |xargs -0 chmod 666) || true
(find . -type f -name "*.signature" -print0 |xargs -0 chmod 666) || true

shopt -s globstar # Enable globstar to support recursive globbing

if [[ -z "${SIGNING_KEY_PYTHON}" ]]; then
    echo "Error: SIGNING_KEY_PYTHON is not set"
    exit 1
fi

if [[ -z "${SIGNING_KEY_LATEX}" ]]; then
    echo "Error: SIGNING_KEY_LATEX is not set"
    exit 1
fi

folders=("camps" "info" "math_days" "preparation_weekend" "zirkel" "playground" "shared_code")
# Iterate over each folder
for folder in "${folders[@]}"; do
    echo "Processing folder: ${folder}"
    if [[ -d "exports/${folder}" ]]; then

        # process a single file

        for tex_file in exports/"${folder}"/**/*.{tex,sty,cls,tdf}; do
            if [[ -f "$tex_file" ]]; then
                echo "        .tex file to sign: ${tex_file}"

                mathezirkel-app-scriptserver-base64 "${tex_file}" "${SIGNING_KEY_LATEX}" --base64-only > "$(dirname "${tex_file}")/$(basename "${tex_file}").base64"
                mathezirkel-app-scriptserver-base64 "${tex_file}" "${SIGNING_KEY_LATEX}" --signature-only > "$(dirname "${tex_file}")/$(basename "${tex_file}").signature"
            fi
        done

        for python_file in exports/"${folder}"/**/*.py; do
            if [[ -f "${python_file}" ]]; then
                echo "        .py file to sign: ${python_file}"

                mathezirkel-app-scriptserver-base64 "${python_file}" "${SIGNING_KEY_PYTHON}" --base64-only > "$(dirname "${python_file}")/$(basename "${python_file}").base64"
                mathezirkel-app-scriptserver-base64 "${python_file}" "${SIGNING_KEY_PYTHON}" --signature-only > "$(dirname ${python_file})/$(basename "${python_file}").signature"
            fi
        done

        for image_file in exports/"${folder}"/**/*.{png,jpeg,jpg}; do
            if [[ -f "${image_file}" ]]; then
                echo "        image file: ${image_file}"

                mathezirkel-app-scriptserver-base64 "${image_file}" nosigningkey --base64-only > "$(dirname "${image_file}")/$(basename "${image_file}").base64"
            fi
        done

        for data_file in exports/"${folder}"/**/*.csv; do
            if [[ -f "${data_file}" ]]; then
                echo "        data file: ${data_file}"

                mathezirkel-app-scriptserver-base64 "${data_file}" nosigningkey --base64-only > "$(dirname "${data_file}")/$(basename "${data_file}").base64"
            fi
        done

        # end processing a single file
    else
        echo "Folder ${folder} does not exist."
    fi
done

# Build generated.json
pkl eval main.pkl -o generated.json
