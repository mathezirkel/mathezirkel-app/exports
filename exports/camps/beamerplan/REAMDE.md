# Beamer allocation

## Description

Due to limited availability of beamers at the camp, their usage must be optimized.

## Constraints

* **Daily Independence**: Beamer usage is managed independently for each day.
* **Instructor Continuity**: Consecutive usage by the same instructor is preferable.
* **Minimized Relocation**: The distance between consecutive uses of the same beamer should be minimized. The distances are defined in a [penalty matrix](./room_change_penalties.csv).

## Problem model

The problem is formulated as a **minimum-cost flow problem** within a layered graph structure.

### Graph structure

```mermaid
flowchart LR
    src((SRC))

    subgraph layer 1
        A1_IN((room A))
        A1_OUT((room A))
        B1_IN((room B))
        B1_OUT((room B))
        X1_IN((unused))
        X1_OUT((unused))
    end

    subgraph layer 2
        A2_IN((room A))
        A2_OUT((room A))
        B2_IN((room B))
        B2_OUT((room B))
        C2_IN((room C))
        C2_OUT((room C))
    end

    dest((DEST))

    src --> A1_IN
    src --> B1_IN
    src --> X1_IN

    A1_IN --> A1_OUT
    B1_IN --> B1_OUT
    X1_IN --> X1_OUT

    A1_OUT --> A2_IN
    A1_OUT --> B2_IN
    A1_OUT --> C2_IN
    B1_OUT --> A2_IN
    B1_OUT --> B2_IN
    B1_OUT --> C2_IN
    X1_OUT --> A2_IN
    X1_OUT --> B2_IN
    X1_OUT --> C2_IN

    A2_IN --> A2_OUT
    B2_IN --> B2_OUT
    C2_IN --> C2_OUT

    A2_OUT --> dest
    B2_OUT --> dest
    C2_OUT --> dest
```

* **Source and destination node**: These nodes mark the start and end of beamer allocation paths.
* **Layers**: Each layer represents a time slot for beamer allocation.
* **Meta-Nodes**: Represent specific beamer locations (requested zirkel or unused beamers) within a layer. Each meta-node comprises two nodes connected by an edge with a capacity of 1, as outlined in [Issue #10](https://gitlab.com/mathezirkel/mathezirkel-app/exports/-/issues/10).
* **Edges**: Consecutive layers are fully connected at the level ov meta-nodes. Each edge has:
  * **Capacity**: 1 (ensuring each path corresponds to one beamer).
  * **Weight**: Defined by the constraints (e.g., penalties for relocation).

### Interpretation

Each flow path through the graph represents the optimal path of one beamer.
