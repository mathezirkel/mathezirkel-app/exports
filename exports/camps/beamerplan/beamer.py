import datetime
from enum import IntEnum
import itertools
import json
from dataclasses import dataclass
import networkx as nx
import pandas as pd

DataDict = dict[str, str | list[dict[str, str | list[str] | None]]]

PENALTY_SAME_INSTRUCTORS = 1

class NodeType(IntEnum):
    IN = 0
    OUT = 1
    UNDEFINED = -1

@dataclass(frozen=True)
class Node:
    number: int
    layer: int
    node_type: NodeType

def calculate_penalty(model: nx.DiGraph, node_a: Node, node_b: Node, penalty_matrix: pd.DataFrame) -> int:
    room_a = model.nodes[node_a].get("room")
    room_b = model.nodes[node_b].get("room")
    instructors_a = set(model.nodes[node_a].get("instructors", []))
    instructors_b = set(model.nodes[node_b].get("instructors", []))

    if instructors_a & instructors_b:
        return PENALTY_SAME_INSTRUCTORS
    if room_a and room_b:
        return penalty_matrix.at[room_a, room_b]
    return 1

def make_problem_model(
    slots: list[DataDict],
    room_change_penalty_matrix: pd.DataFrame,
    number_of_available_beamers: int,
) -> nx.DiGraph:
    model = nx.DiGraph()
    src_node = Node(number=-1, layer=0, node_type=NodeType.UNDEFINED)
    model.add_node(src_node)
    for layer, slot in enumerate(slots, start=1):
        free_beamer = number_of_available_beamers - len(slot["zirkelPlanEntries"])
        for i, entry in enumerate(
            slot["zirkelPlanEntries"] + [{"room": "Teamzimmer"}] * free_beamer,
            start=1
        ):
            # We split each nodes into two nodes connected with an edge of capacity 1 to avoid 2 beamers in the same room at the same time
            current_node_in = Node(number=i, layer=layer, node_type=NodeType.IN)
            current_node_out = Node(number=i, layer=layer, node_type=NodeType.OUT)
            model.add_node(current_node_in, **entry)
            model.add_node(current_node_out, **entry)
            model.add_edge(current_node_in, current_node_out, capacity=1, weight=1)
            # Connect current layer to the previous layer
            for predecessor_node in (
                node for node in model.nodes
                if node.layer == layer-1 and node.node_type in [NodeType.OUT, NodeType.UNDEFINED]
            ):
                penalty = calculate_penalty(model, predecessor_node, current_node_in, room_change_penalty_matrix)
                model.add_edge(predecessor_node, current_node_in, capacity=1, weight=penalty)
    # Destination node
    dest_node = Node(number=-1, layer=len(slots)+1, node_type=NodeType.UNDEFINED)
    model.add_node(dest_node)
    for predecessor_node in (node for node in model.nodes if node.layer == len(slots) and node.node_type == NodeType.OUT):
        model.add_edge(predecessor_node, dest_node, capacity=1, weight=1)
    return model

def get_beamer_allocation(
    slots: list[DataDict],
    room_change_penalty_matrix: pd.DataFrame,
    number_of_available_beamers: int,
) -> list[list[dict[str, str | list[str]]]]:
    model = make_problem_model(slots, room_change_penalty_matrix, number_of_available_beamers)
    src_node = Node(number=-1, layer=0, node_type=NodeType.UNDEFINED)
    dest_node = Node(number=-1, layer=len(slots)+1, node_type=NodeType.UNDEFINED)
    flow_dict: dict[Node, dict[Node, int]] = nx.max_flow_min_cost(model, src_node, dest_node)
    beamer_allocation = nx.edge_subgraph(
        model,
        [(u, v) for u, neighbors in flow_dict.items() for v, flow in neighbors.items() if flow > 0]
    )
    beamer_plan = []
    for path in nx.all_simple_paths(beamer_allocation, src_node, dest_node):
        beamer_plan.append([
            {
                "zirkel": beamer_allocation.nodes[node].get("zirkel", ""),
                "room": beamer_allocation.nodes[node].get("room", ""),
                "instructors": beamer_allocation.nodes[node].get("instructors", []),
            }
            for node in path if node.node_type == NodeType.IN
        ])
    return sorted(
        beamer_plan,
        key=lambda day: tuple(slot["room"] == "Teamzimmer" for slot in day)
    )

def make_tabular_header(number_of_available_beamers: int) -> str:
    return " & ".join(
        [
            r"\textbf{Slot}"
        ] + [
            rf"\textbf{{Beamer {i+1}}}" for i in range(number_of_available_beamers)
        ]
    ) + r"\\"

def make_cell_tex(cell: dict[str, str | list[str]]):
    if cell["room"] == "Teamzimmer":
        return ""
    return "\n".join([
        r"\begin{tabular}{@{}l@{}}",
        rf"{cell['zirkel']}\\",
        rf"{', '.join(cell['instructors'])}\\",
        rf"{cell['room']}\\",
        r"\end{tabular}"
    ])

def make_beamer_plan_on_day(
    slots_on_day: list[DataDict],
    room_change_penalty_matrix: pd.DataFrame,
    availaible_beamer: int
) -> str:
    beamer_allocation_on_day = get_beamer_allocation(
        slots_on_day,
        room_change_penalty_matrix,
        availaible_beamer
    )
    return "\n\\\\\\hline\n".join(
        "\n&\n".join(
            [
                slot["name"]
            ] + [
                make_cell_tex(beamer[i])
                for beamer in beamer_allocation_on_day
            ]
        )
        for i, slot in enumerate(slots_on_day)
    ) + "\n\\\\\\hline[2pt]\n"

def get_slot_date(zirkel_plan_slot: DataDict) -> datetime.date:
    return datetime.datetime.strptime(zirkel_plan_slot["startTime"], "%Y-%m-%d %H:%M:%S").date()

def make_beamer_plan(beamer_requests: list[DataDict], number_of_available_beamers: int) -> str:
    number_of_needed_beamers = max(len(slot["zirkelPlanEntries"]) for slot in beamer_requests)
    if number_of_needed_beamers > number_of_available_beamers:
        slots_with_too_may_beamer_requests = [
            slot["name"]
            for slot in beamer_requests
            if len(slot["zirkelPlanEntries"]) > number_of_available_beamers
        ]
        return r"\\".join([
            "Nicht genug Beamer!",
            f"Benötigt: {number_of_needed_beamers}",
            f"Verfügbar: {number_of_available_beamers}",
            f"Betroffene Slots: {', '.join(slots_with_too_may_beamer_requests)}",
        ])
    room_change_penalty_matrix = pd.read_csv("room_change_penalties.csv", sep=';', index_col=0)
    return "\n".join(
        [
            r"\begin{longtblr}{",
            rf"colspec={{|l|{'X|' * number_of_available_beamers}}},",
            r"width=\textwidth,",
            "rowhead=1,",
            "row{odd} = {gray!20},",
            "row{even} = {white},",
            "row{1} = {gray!40}",
            "}",
            r"\hline",
            make_tabular_header(number_of_available_beamers),
            r"\hline",
        ] + [
                make_beamer_plan_on_day(
                    [slot for slot in slots_on_day if "Nachmittag" not in slot["name"]],
                    room_change_penalty_matrix,
                    number_of_available_beamers
                )
                for _, slots_on_day in itertools.groupby(beamer_requests, key=get_slot_date)
        ] + [
            r"\end{longtblr}"
        ]
    )

def write_beamer_plan(data: dict[str, int | list[DataDict]]) -> None:
    number_of_available_beamers = data["numberOfAvailableBeamers"] or 0
    beamer_requests = data["data"]
    with open("content.tex", "w", encoding="utf-8") as tex_file:
        tex_string = make_beamer_plan(beamer_requests, number_of_available_beamers)
        tex_file.write(tex_string)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : dict[str, int | list[DataDict]] = json.load(data_file)
    write_beamer_plan(data)

if __name__ == "__main__":
    main()
