# Build data dict
.data.event |
{
  numberOfAvailableBeamers,
  "data":
    [
      .zirkelPlanSlots[] |
      select(.beamer) |
      {
          name,
          startTime,
          "zirkelPlanEntries":
            [
              .zirkelPlanEntries[] |
              select(.beamer) |
              {
                  "zirkel": .zirkel.name,
                  "room": .zirkel.room,
                  "instructors": [
                    .instructorExtensionsPublic[] |
                    .name
                  ],
              }
            ],
      }
    ] |
    sort_by(.startTime)
}
