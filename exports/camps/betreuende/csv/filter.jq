# Build data dict
[
  .data.event.instructorExtensions[] |
  {
    "Vorname": .instructor.callName,
    "Nachname": .instructor.familyName,
  }
] |

# Sort data
sort_by(.Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")

