# Build data dict
{
  "columns":
    {
        "blank": {
            "align": "X"
        }
    },
  "rows":
    [
      .data.event.instructorExtensions[] |
      {
        "Vorname": .instructor.callName,
        "Nachname": .instructor.familyName,
        "blank": "",
      }
    ] |

    # Sort data
    sort_by(.Vorname, .Nachname),
}
