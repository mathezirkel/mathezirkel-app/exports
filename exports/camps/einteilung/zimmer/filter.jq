# Build data dict
.data.event |
{
  "mode": "hallway",
  "sortBy": "roomNumber",
  "columnWidth": "4cm",
  "maxColumns": 6,
  "data": (
    (
      [
        .extensions[] |
        select(.confirmed == true and .timeOfSignoff == null)
      ] |
      group_by (.roomNumber) |
      map(
        {
          "key": (.[0].roomNumber // "TBD"),
          "members": (
            map(
              {
                "callName": .participant.callName,
                "familyName": .participant.familyName,
                "classYear": (.classYear | sub("CLASS";"")),
              }
            ) |
            sort_by(.callName, .familyName)
          )
        }
      )
    ) + (
      .instructorExtensions |
      group_by(.roomNumber) |
      map(
        {
          "key": (.[0].roomNumber // "TBD"),
          "extra": "Betreuende",
          "members": (
            map(
              {
                "callName": .instructor.callName,
                "familyName": (.instructor.familyName[0:1] + "."),
              }
            ) |
            sort_by(.callName, .familyName)
          )
        }
      )
    )
  )
}
