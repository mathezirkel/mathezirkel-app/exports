# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null)
] |
group_by (.nutrition) |
map(
  {
    "key": (.[0].nutrition // "UNKNOWN"),
    "value": length,
  }
) |

# Build output
from_entries
