# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  select(.carpoolDataSharing == true) |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
    "PLZ": (.participant.postalCode // ""),
    "Ort": (.participant.city // ""),
    "Land": (.participant.country // ""),
    "E-Mail Schueler:in": (.emailSelf // ""),
    "Weitere E-Mails": (.additionalEmails | join(", ")),
    "Telefon": (.telephone // ""),
  }
] |

# Sort data
sort_by(.Land, .PLZ, .Nachname, .Vorname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
