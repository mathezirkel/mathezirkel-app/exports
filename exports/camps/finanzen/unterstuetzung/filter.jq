# Get default fee
.data.event.defaultFee as $defaultFee |

# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  select(.fee < $defaultFee) |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
    "Camppreis": .fee,
    "Reduktion": ($defaultFee - .fee),
  }
] |

# Sort data
sort_by(.Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
