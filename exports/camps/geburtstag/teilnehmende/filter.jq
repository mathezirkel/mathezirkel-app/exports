
# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  select(.hasBirthday == true) |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
    "Geburtsdatum": .participant.birthDate,
    "Klasse": (.classYear | sub("CLASS";"")),
  }
] |

# Sort data
sort_by((.Geburtsdatum | sub("^...."; "")), .Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")

