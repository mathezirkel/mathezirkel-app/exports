# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  {
    "postalCode": .participant.postalCode,
    "city": .participant.city,
    "country": .participant.country,
  }
] + [
  .data.event.instructorExtensions[] |
  {
    "postalCode": .instructor.postalCode,
    "city": .instructor.city,
    "country": .instructor.country,
  }
] |

# Sort data
sort_by(.country, .postalCode, .city)
