# Build data dict
[
  .data.event.instructorExtensions[] |
  {
    "callName": .instructor.callName,
    "familyName": .instructor.familyName,
    "roomNumber": (.roomNumber // ""),
  }
] |

# Sort data
sort_by(.callName, .familyName) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
