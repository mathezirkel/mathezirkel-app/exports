# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  {
    "callName": .participant.callName,
    "familyName": .participant.familyName,
    "zirkel": (.zirkel | map(.name) | join(", ")),
    "zirkelRoom": (.zirkel | map(.room) | join(", ")),
    "roomNumber": (.roomNumber // ""),
    "arrival": (.arrival // ""),
  }
] |

# Sort data
sort_by(.arrival, .callName, .familyName) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
