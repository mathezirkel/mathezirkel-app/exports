# Build data dict
.data.event.zirkelPlanSlots |
sort_by(.startTime) |
.[0] |
.zirkelPlanEntries |
map(
  {
    "name": .zirkel.name,
    "room": .zirkel.room,
    "instructors": (
      .instructorExtensionsPublic |
      map(.name) |
      sort
    ),
    "participants": (
      .zirkel.extensions |
      map(select(.confirmed == true and .timeOfSignoff == null)) |
      map(.participant) |
      sort_by(.callName, .familyName)
    ),
  }
)
