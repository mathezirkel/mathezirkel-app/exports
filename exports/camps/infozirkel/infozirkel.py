import json
import re

DataDict = dict[str, None | str | list[str] | list[dict[str, str]]]

class ZirkelSorter:
    compiled_regex = re.compile(r"([5-9A-Z]|1[0-3])([5-9A-Z]|1[0-3])?([a-z]?)")

    @classmethod
    def _sort_key(cls, zirkel: DataDict) -> tuple[int, int | str]:
        key = zirkel["name"]
        match = cls.compiled_regex.match(key)
        if match:
            return tuple(
                ((0, int(x)) if x.isdigit() else (1, x)) if x else (0,0)
                for x in match.groups()
            )
        return ((1,str(key)), (0,0), (0,0))

    @classmethod
    def sorted(cls, zirkels: list[DataDict]) -> list[DataDict]:
        return sorted(zirkels, key=cls._sort_key)

def get_level(zirkel: str) -> str:
    match = ZirkelSorter.compiled_regex.match(zirkel)
    if match:
        if match.groups()[0] in ["5", "6"]:
            return "Unterstufe"
        if match.groups()[0] in ["7", "8", "9"]:
            return "Mittelstufe"
    return "Oberstufe"

def make_participants_list(participants: list[dict[str, str]]) -> str:
    number_of_participant_colums = len(participants) // 12
    participants_string = "\n".join(
        rf"\item {participant['callName']} {participant['familyName']}"
        for participant in participants
    )
    participants_list_string = "\n".join([
        r"\begin{itemize}",
        participants_string,
        r"\end{itemize}",
    ])
    if number_of_participant_colums > 1:
        return "\n".join([
            rf"\begin{{multicols}}{{{number_of_participant_colums}}}",
            participants_list_string,
            r"\end{multicols}",
        ])
    return participants_list_string

def make_infozirkel(infozirkel: DataDict) -> str:
    name = infozirkel["name"]
    room = infozirkel["room"]
    instructors = ", ".join(infozirkel["instructors"])
    participants = infozirkel["participants"]
    return "\n".join([
        r"\setcounter{page}{1}",
        r"\setcounter{section}{0}",
        rf"\renewcommand{{\level}}{{{get_level(name)}}}",
        r"{\huge \textbf{Infozirkel}}",
        r"\vspace{2em}",
        "",
        r"\begin{tabular}{ll}",
        rf"Zirkel: & {name}\\",
        rf"Betreuer:innen: & {instructors}\\",
        rf"Raum: & {room}",
        r"\end{tabular}",
        r"\section{Anwesenheit überprüfen}",
        make_participants_list(participants),
        r"\input{infozirkel_content.tex}"
    ])

def write_infozirkel_file(data: list[DataDict]) -> None:
    with open("content.tex", "w", encoding="utf-8") as tex_file:
        tex_string = "\n".join([
            r"\documentclass[ngerman, a4paper, twoside]{scrartcl}",
            r"\usepackage[ngerman]{babel}",
            r"\usepackage[utf8]{inputenc}",
            r"\usepackage[T1]{fontenc}",
            r"\usepackage{lmodern}",
            r"\usepackage{enumitem}",
            r"\usepackage{multicol}",
            r"\usepackage{xcolor}",
            r"\usepackage{ifthen}",
            r"\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm,footskip=1cm]{geometry}",
            r"\setlength{\parindent}{0pt}",
            r"\newcommand{\level}{}",
            r"\begin{document}",
            "\n\\cleardoublepage\n".join([
                make_infozirkel(infozirkel) for infozirkel in data if infozirkel["participants"]
            ]),
            r"\end{document}",
        ])
        tex_file.write(tex_string)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : list[DataDict] = json.load(data_file)
    write_infozirkel_file(ZirkelSorter.sorted(data))

if __name__ == "__main__":
    main()
