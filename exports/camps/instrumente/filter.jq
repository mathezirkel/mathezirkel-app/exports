# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  select(.instruments != []) |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
    "Klasse": (.classYear | sub("CLASS";"")),
    "Instrumente": (.instruments | sort | join(", ")),
    "E-Mail Schueler:in": (.emailSelf // ""),
    "Weitere E-Mails": (.additionalEmails | join(", ")),
  }
] |

# Sort data
sort_by(.Instrumente, .Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
