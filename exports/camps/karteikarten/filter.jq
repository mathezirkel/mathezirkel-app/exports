# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  {
    participant,
    roomNumber,
    zirkel,
    foodRestriction,
    medicalNotes,
    notes,
    pool,
    leavingPremise,
    signupPhoto,
  }
] |
sort_by(.participant.callName, .participant.familyName)
