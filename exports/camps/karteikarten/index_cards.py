import base64
from io import BytesIO
import json
from pathlib import Path
import re
from PIL import Image

DataDict = dict[str, str | dict[str,str] | list[str] | list[dict[str,str]]]

EXPORT_DIR = Path("exports")
IMAGE_DIR = EXPORT_DIR / "img"
PICTURE_DUMMY = "zirkelgregor-quadratisch.png"
IMAGE_QUALITY = 50

def get_image_filename(family_name: str, call_name: str) -> str:
    special_char_map = {ord('ä'): 'ae', ord('ü'): 'ue',
                        ord('ö'): 'oe', ord('ß'): 'ss'}
    image_filename =  (
        (family_name + call_name)
        .translate(special_char_map)
        .replace(" ", "")
        .lower()
    )
    return f"{image_filename}"

def decode_base_64_image(base64_image_string: str) -> bytes:
    match = re.match(r'data:image/[^;]+;base64,(.*)', base64_image_string)
    if match:
        base64_data = match.group(1)
        image_data = base64.b64decode(base64_data)
        return image_data
    return None, ""

def save_image(image_data: bytes | None, image_folder: Path, filename: str) -> None:
    if image_data:
        image = Image.open(BytesIO(image_data)).convert("RGB")
        image.save(
            image_folder / f"{filename}.jpg",
            "JPEG",
            quality=IMAGE_QUALITY,
            optimize=True,
        )

def checkbox(value: str) -> str:
    if value:
        return r"\checkedBox"
    return r"\uncheckedBox"

def make_participant(data: DataDict, image_folder: Path) -> str:
    family_name = data.get("participant", {}).get("familyName", "")
    call_name = data.get("participant", {}).get("callName", "")
    if not family_name and not call_name:
        return ""
    image_data = decode_base_64_image(data.get("signupPhoto", "") or "")
    if image_data:
        image_filename = get_image_filename(family_name, call_name)
        save_image(image_data, image_folder, image_filename)
        image_path = str(image_folder / f"{image_filename}.jpg")
    else:
        image_path = PICTURE_DUMMY
    tex_picture_code = rf"\includegraphics[width=0.9\textwidth]{{{image_path}}}"
    return "\n".join([
        r"\begin{minipage}{0.35\textwidth}",
        tex_picture_code,
        r"\end{minipage}",
        r"\begin{minipage}{0.6\textwidth}",
        r"\rowcolors{1}{gray!20}{white}",
        r"\begin{tabular}{@{}p{0.35\textwidth}p{0.65\textwidth}@{}}",
        rf"Vorname: & {call_name} \\",
        rf"Nachname: & {family_name} \\",
        rf"Geschlecht: & {data.get('participant', {}).get('gender', '')} \\",
        rf"Zirkel: & {', '.join(name for zirkel in data.get('zirkel', []) if (name := zirkel.get('name', '')))} \\"
        rf"Zimmer: & {data.get('roomNumber', '') or ''} \\",
        rf"Unverträglichkeiten: & {data.get('foodRestriction', '') or ''} \\",
        rf"Medizinische Infos: & {', '.join(data.get('medicalNotes', []))} \\",
        # rf"Pool: & {checkbox(data.get('pool', False))} \\",
        rf"Gelände verlassen: & {checkbox(data.get('leavingPremise', False))} \\",
        rf"Sonstiges: & {data.get('notes', '') or ''} \\",
        r"\end{tabular}",
        r"\end{minipage}",
    ])

def write_index_cards(data: list[DataDict], image_folder: Path) -> None:
    with open(EXPORT_DIR / "content.tex", "w", encoding="utf-8") as tex_file:
        tex_string = "\n\\clearpage\n".join(
            make_participant(participant, image_folder)
            for participant in data
        )
        tex_file.write(tex_string)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : list[DataDict] = json.load(data_file)
    EXPORT_DIR.mkdir(exist_ok=True)
    IMAGE_DIR.mkdir(exist_ok=True)
    write_index_cards(data, IMAGE_DIR)

if __name__ == "__main__":
    main()
