# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  {
    "callName": .participant.callName,
    "familyName": .participant.familyName,
    "address": {
      "street": .participant.street,
      "streetNumber": .participant.streetNumber,
      "postalCode": .participant.postalCode,
      "city": .participant.city,
      "country": .participant.country
    },
    emailSelf,
    additionalEmails,
    telephone,
    contacts,
    "classYear": (.classYear | sub("CLASS";"")),
  }
]

