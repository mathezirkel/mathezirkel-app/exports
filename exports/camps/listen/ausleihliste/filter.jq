# Build data dict
{
  "columns": {
    "Ausgeliehen": {
        "align": "X"
    }
  },
  "arraystretch": 2.2,
  "rows":
    [
      .data.event.extensions[] |
      select(.confirmed == true and .timeOfSignoff == null) |
      {
        "Vorname": .participant.callName,
        "Nachname": .participant.familyName,
        "Ausgeliehen": "",
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
