# Build data dict
{
  "columns": {
    "Getränke": {
        "align": "X"
    }
  },
  "arraystretch": 1.5,
  "rows":
    [
      .data.event.instructorExtensions[] |
      {
        "Vorname": .instructor.callName,
        "Nachname": .instructor.familyName,
        "Getränke": "",
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
