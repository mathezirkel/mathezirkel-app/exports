# Build data dict
{
  "columns": {
      "Getränke": {
          "align": "X"
      }
  },
  "arraystretch": 1.5,
  "rows":
    [
      .data.event.extensions[] |
      select(.confirmed == true and .timeOfSignoff == null) |
      {
        "Vorname": .participant.callName,
        "Nachname": .participant.familyName,
        "Getränke": "",
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
