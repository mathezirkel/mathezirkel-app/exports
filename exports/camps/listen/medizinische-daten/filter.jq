# Build data dict
{
  "columns": {
      "Medizinische Infos": {
          "align": "X"
      },
      "Unverträglichkeiten": {
          "align": "X"
      }
  },
  "rows":
    [
      .data.event.extensions[] |
      select(.confirmed == true and .timeOfSignoff == null) |
      select(.medicalNotes != [] or .foodRestriction != "") |
      {
        "Vorname": .participant.callName,
        "Nachname": .participant.familyName,
        "Klasse": (.classYear | sub("CLASS";"")),
        "Zirkel": (
          .zirkel |
          map(
            .name |
            if (. | startswith("Q"))
            then "Q"
            else .
            end
          ) |
          join(", ")
        ),
        "Zimmer": (.roomNumber // ""),
        "Medizinische Infos": (.medicalNotes | join(", ")),
        "Unverträglichkeiten": (.foodRestriction // ""),
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
