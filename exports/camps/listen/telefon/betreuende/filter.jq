# Build data dict
{
  "columns": {
    "Telefon": {
        "type": "telephone"
    }
  },
  "rows":
    [
      .data.event.instructorExtensions[] |
      {
        "Vorname": .instructor.callName,
        "Nachname": .instructor.familyName,
        "Telefon": (.instructor.telephone // ""),
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
