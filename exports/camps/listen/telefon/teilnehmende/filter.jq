# Build data dict
{
  "columns": {
    "Telefon": {
      "type": "telephone"
    },
    "Notfallnummern": {
      "type": "telephone",
      "multi": true
    }
  },
  "rows":
    [
      .data.event.extensions[] |
      select(.confirmed == true and .timeOfSignoff == null) |
      {
        "Vorname": .participant.callName,
        "Nachname": .participant.familyName,
        "Geschlecht": .participant.gender,
        "Telefon": (.telephone // ""),
        "Notfallnummern": [.contacts[].telephone],
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
