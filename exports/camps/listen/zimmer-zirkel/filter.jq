# Build data dict
{
  "rows":
    [
      .data.event.extensions[] |
      select(.confirmed == true and .timeOfSignoff == null) |
      {
        "Vorname": .participant.callName,
        "Nachname": .participant.familyName,
        "Geschlecht": .participant.gender,
        "Klasse": (.classYear | sub("CLASS";"")),
        "Zirkel": (
          .zirkel |
          map(
            .name |
            if (. | startswith("Q"))
            then "Q"
            else .
            end
          ) |
          join(", ")
        ),
        "Zimmer": (.roomNumber // ""),
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
