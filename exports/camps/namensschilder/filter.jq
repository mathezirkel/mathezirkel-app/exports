# Build data dict
.data.event |

# Participants
.extensions |= map(
  select(.confirmed == true and .timeOfSignoff == null) |
  { participant }
) |
.extensions |= sort_by(.participant.callName) |

# Instructors
.instructorExtensions |= sort_by(.instructor.callName)
