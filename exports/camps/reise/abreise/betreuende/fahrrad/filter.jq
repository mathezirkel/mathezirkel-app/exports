# Build data dict
{
  "columns": {
    "blank_1": {
      "align": "c",
      "escape": false
    },
    "blank_2": {
      "align": "c",
      "escape": false
    },
    "Telefon": {
      "type": "telephone"
    },
    "Bemerkung": {
      "align": "X"
    }
  },
  "rows":
    [
      .data.event.instructorExtensions[] |
      select(.departure == "BICYCLE") |
      {
        "blank_1": "$\\square$",
        "blank_2": "$\\square$",
        "Vorname": .instructor.callName,
        "Nachname": .instructor.familyName,
        "Telefon": (.instructor.telephone // ""),
        "Bemerkung": (.departureNotes // ""),
      }
    ] |
    sort_by(.Vorname, .Nachname)
}
