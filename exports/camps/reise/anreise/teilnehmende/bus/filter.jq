# Build data dict
{
  "columns": {
    "blank_1": {
      "align": "c",
      "escape": false
    },
    "blank_2": {
      "align": "c",
      "escape": false
    },
    "Notfallnummern": {
      "type": "telephone",
      "multi": true
    },
    "Bemerkung": {
      "align": "X"
    }
  },
  "rows":
    [
      .data.event.extensions[] |
      select(.confirmed == true and .timeOfSignoff == null) |
      select(.arrival == "BUS") |
      {
        "blank_1": "$\\square$",
        "blank_2": "$\\square$",
        "Vorname": .participant.callName,
        "Nachname": .participant.familyName,
        "Klasse": (.classYear | sub("CLASS";"")),
        "Notfallnummern": [.contacts[].telephone],
        "Bemerkung": (.arrivalNotes // ""),
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
