# Build data dict
.data.event |
{
  "instructors":
    (
      [
          .instructorExtensionsPublic[] |
          .name
      ] |
      sort
    ),
  "zirkelPlanSlots":
    (
      .zirkelPlanSlots |
      sort_by(.startTime)
    ),
}
