import datetime
import json

ZirkelDict = dict[str, None | str]
InstructorDict = dict[str, None | str]
ZirkelPlanEntryDict = dict[str, None | bool | str | ZirkelDict | list[InstructorDict]]
ZirkelPlanSlotDict = dict[str, str | list[ZirkelPlanEntryDict]]
DataDict = dict[str, list[str] | list[ZirkelPlanSlotDict]]

def make_timetable_row(
    instructor: str,
    slot: ZirkelPlanSlotDict,
):
    slot_name = slot["name"]
    start_time = datetime.datetime.strptime(slot["startTime"], "%Y-%m-%d %H:%M:%S").strftime("%H.%M")
    end_time = datetime.datetime.strptime(slot["endTime"], "%Y-%m-%d %H:%M:%S").strftime("%H.%M")
    zirkel_plan_entry = next(
        (
            entry for entry in slot["zirkelPlanEntries"]
            if instructor in [d["name"] for d in entry["instructorExtensionsPublic"]]
        ),
        {}
    )
    zirkel = zirkel_plan_entry.get("zirkel", {}).get("name", "") or ""
    room = zirkel_plan_entry.get("zirkel", {}).get("room", "") or ""
    other_instructors = ", ".join([
        d["name"]
        for d in zirkel_plan_entry.get("instructorExtensionsPublic", [])
        if d["name"] != instructor
    ])
    topic = zirkel_plan_entry.get("topic", "") or ""
    beamer = r"\beamer" if zirkel_plan_entry.get("beamer", False) else ""
    return " & ".join([
        slot_name,
        f"{start_time} -- {end_time} Uhr",
        zirkel,
        other_instructors,
        room,
        beamer,
        topic,
    ]) + r"\\"

def make_timetable_rows(
    instructor: str,
    zirkel_plan_slots: list[ZirkelPlanSlotDict],
) -> str:
    return "\n\\hline\n".join([
        make_timetable_row(instructor, slot)
        for slot in zirkel_plan_slots
    ]) + "\n\\hline\n"

def make_timetable_instructor(
    instructor: str,
    zirkel_plan_slots: list[ZirkelPlanSlotDict],
) -> str:
    return "\n".join([
        r"\begin{center}",
        r"\Huge\textsc{Stundenplan}",
        r"\end{center}",
        r"\begin{tabular}{ll}",
        rf"\textbf{{Name:}} & {instructor}",
        r"\end{tabular}",
        r"\begin{center}",
        r"\rowcolors{1}{gray!20}{white}",
        r"\begin{xltabular}{\textwidth}{|l|l|c|l|lc|X|}",
        r"\hline",
        r"\rowcolor{gray!40}",
        r"\textbf{Slot} & \textbf{Zeit} & \textbf{Zirkel} & \textbf{Mit} & \textbf{Raum} & & \textbf{Thema}\\",
        r"\hline",
        r"\endhead",
        make_timetable_rows(instructor, zirkel_plan_slots),
        r"\end{xltabular}",
        r"\end{center}",
    ])

def write_timetables(data: dict[str, int | list[DataDict]]) -> None:
    instructors = data["instructors"]
    zirkel_plan_slots = data["zirkelPlanSlots"]
    with open("content.tex", "w", encoding="utf-8") as tex_file:
        tex_string = "\n\\cleardoublepage\n".join(
            make_timetable_instructor(instructor, zirkel_plan_slots)
            for instructor in instructors
        )
        tex_file.write(tex_string)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : dict[str, int | list[DataDict]] = json.load(data_file)
    write_timetables(data)

if __name__ == "__main__":
    main()
