# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
  }
] |

# Sort data
sort_by(.Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
