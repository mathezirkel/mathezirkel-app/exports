# Build data dict
{
  "columns": {
    "blank": {
      "align": "X"
    }
  },
  "rows":
    [
      .data.event.extensions[] |
      select(.confirmed == true and .timeOfSignoff == null) |
      {
        "Vorname": .participant.callName,
        "Nachname": .participant.familyName,
        "blank": "",
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
