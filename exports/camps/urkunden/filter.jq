.data.event |
.startDate as $startDate |
.certificateSignatureDate = (.certificateSignatureDate // .endDate) |
.zirkel |= (
  map(
    .name = (
      if (.name // "" | startswith("Q"))
      then "Q"
      else .name // ""
      end
    )
  ) |
  group_by(.name) |
  map(
    {
      name: .[0].name,
      topics: map(.topics) | add | map(gsub("^\\s+|\\s+$";"")) | unique | sort,
      instructorExtensions: map(.instructorExtensions) | add | unique,
      extensions: (
        map(.extensions) |
        add |
        map(select(.confirmed == true and (.timeOfSignoff == null or .timeOfSignoff > $startDate)))
      ),
    }
  ) |
  sort_by(.name)
)
