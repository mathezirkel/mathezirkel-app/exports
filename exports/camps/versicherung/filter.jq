# Build data dict for participants
(
  [
    .data.event.extensions[] |
    select(.confirmed == true and .timeOfSignoff == null) |
    {
      "Vorname": .participant.givenName,
      "Nachname": .participant.familyName,
      "Geburtstag": .participant.birthDate,
      "Betreuer": "Nein",
    }
  ] |

  # Sort data
  sort_by(.Nachname, .Vorname)
)
+
(
  [
    .data.event.instructorExtensions[] |
    {
      "Vorname": .instructor.givenName,
      "Nachname": .instructor.familyName,
      "Geburtstag": .instructor.birthDate,
      "Betreuer": "Ja",
    }
  ] |
  sort_by(.Nachname, .Vorname)
) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
