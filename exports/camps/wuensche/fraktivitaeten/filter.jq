# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null)
] |

# Count fractivity Wishes
[ .[] | .fractivityWishes ] |
add |
reduce .[] as $item ({}; .[$item] += 1) |

# Sort data
to_entries |
sort_by(.key) | reverse |
sort_by(.value) | reverse |

# Build output
from_entries
