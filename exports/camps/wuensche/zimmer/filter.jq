
# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  select(.roomPartnerWishes != []) |
  {
    "callName": .participant.callName,
    "familyName": .participant.familyName,
    "wishes": .roomPartnerWishes,
    "classYear": (.classYear | sub("CLASS";"")),
  }
]

