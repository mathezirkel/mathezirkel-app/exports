
# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  select(.zirkelPartnerWishes != []) |
  {
    "callName": .participant.callName,
    "familyName": .participant.familyName,
    "wishes": .zirkelPartnerWishes,
    "classYear": (.classYear | sub("CLASS";"")),
  }
]

