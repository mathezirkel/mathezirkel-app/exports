# Build data dict
.data.event.instructorExtensions |
group_by (.roomNumber) |
map(
  {
    "key": (.[0].roomNumber // "TBD"),
    "isParticipant": false,
    "members": (
      map(
        {
          callName: .instructor.callName,
          familyName: (.instructor.familyName[0:1] + "."),
          sex: .instructor.sex,
        }
      ) |
      sort_by(.callName, .familyName)
    )
  }
)

