import json

CellDict = dict[str, str | list[dict[str,str]]]

def sort(data: list[CellDict]) -> list[CellDict]:
    def sort_key(cell: CellDict) -> tuple[int, int | str]:
        key = cell["key"]
        if key.isdigit():
            return (0, int(key))
        return (1, key)

    return sorted(data, key=sort_key)

def get_class_range(members: list[dict[str,str]]) -> str:
    min_class = min(int(member["classYear"]) for member in members)
    max_class = max(int(member["classYear"]) for member in members)
    if min_class == max_class:
        return f"Klasse {str(min_class)}"
    return f"Klasse {min_class} -- {max_class}"

def get_knocking_string(is_room_10: bool) -> str:
    if is_room_10:
        return r"\textsc{Diese Tür einfach öffnen! \\[0.5em] Innen bitte fest klopfen!}"
    return r"\textsc{Bitte einfach klopfen!}"

def make_member_list(members: list[dict[str,str]], is_itemize: bool = False, is_room_10: bool = False) -> str:
    if is_room_10:
        output = []
        for side, sex in [("links", "FEMALE"), ("rechts", "MALE")]:
            output += [
                r"\begin{minipage}{0.45\textwidth}",
                r"\centering",
                rf"\textsc{{{side}}}\\",
                make_member_list(
                    (member for member in members if member["sex"] == sex),
                    is_itemize
                ),
                r"\end{minipage}",
            ]
    else:
        prefix = r"\item" if is_itemize else ""
        suffix = "" if is_itemize else r"\\"
        output = [
            rf"{prefix} {member['callName']} {member['familyName']}{suffix}"
            for member in members
        ]
    return "\n".join(output)

def make_sign(page: CellDict) -> str:
    is_room_10 = page["key"] == "10"
    is_participant: str = page["isParticipant"]
    output = [
        r"\Huge",
        r"\begin{center}",
        "{",
        r"\fontsize" + ("{120}{120}" if is_participant else r"{70}{40}"),
        r"\selectfont",
        "" if is_participant else r"Betreuer:innenzimmer\\[0.5em]",
        page["key"],
        "}"
    ]
    if is_participant:
        output += [
            r"\\",
            get_class_range(page['members']),
            r"\end{center}",
            r"\begin{itemize}",
            make_member_list(page["members"], True, is_room_10),
            r"\end{itemize}",
        ]
    else:
        output += [
            r"\vfill",
            make_member_list(page["members"], False ,is_room_10),
            r"\vfill",
            "{",
            r"\fontsize{40}{40}\selectfont",
            get_knocking_string(is_room_10),
            "}",
            r"\end{center}",
        ]
    return "\n".join(output)

def write_room_signs(data: list[CellDict]) -> None:
    with open("content.tex", "w", encoding="utf-8") as tex_file:
        tex_string = "\n\\clearpage\n".join(
            make_sign(page) for page in sort(data)
        )
        tex_file.write(tex_string)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : list[CellDict] = json.load(data_file)
    write_room_signs(data)

if __name__ == "__main__":
    main()
