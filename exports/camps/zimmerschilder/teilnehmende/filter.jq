# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null)
] |
group_by (.roomNumber) |
map(
  {
    "key": (.[0].roomNumber // "TBD"),
    "isParticipant": true,
    "members": (
      map(
        {
          callName: .participant.callName,
          familyName: .participant.familyName,
          classYear: (.classYear | sub("CLASS";"")),
        }
      ) |
      sort_by(.callName, .familyName)
    )
  }
)
