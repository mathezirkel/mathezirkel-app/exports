# Build data dict
.data.event |
{
  "columnWidth": 5.3,
  "numberOfColumnsPerPage": 5,
  "printBeamer": false,
  "printFreeInstructors": false,
  "zirkelPlanSlots": (
      .zirkelPlanSlots |
      map(
        select(
          (.name | contains("Infozirkel")) or (.name | contains("Aufräumen")) |
          not
      )
    )
  ),
  "zirkel": (
    .zirkel |
    map(select(.name | contains("Q")))
  ),
  instructorExtensionsPublic,
}
