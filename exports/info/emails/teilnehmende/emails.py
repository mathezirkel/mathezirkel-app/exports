import csv
import datetime
import json

MAXIMUM_CLASS_YEAR = 12

DataDict = dict[str, str | list[dict[str, str | int]]]

def get_estimated_class_year(extensions: list[dict[str, str | int]]) -> int | str:
    if len(extensions) == 1 and extensions[0]["classYear"] == "_INFO":
        return "_INFO"
    # From September, the class year will be calculated based on the new zirkel year.
    current_year = datetime.datetime.now().year + (1 if datetime.datetime.now().month >= 9 else 0)
    year_of_newest_extension = max(
        extension["eventYear"] for extension in extensions
        if extension["classYear"] != "_INFO"
    )
    maximum_class_year_of_newest_extensions = max(
        int(extension["classYear"]) for extension in extensions
        if extension["eventYear"] == year_of_newest_extension
    )
    return maximum_class_year_of_newest_extensions + (current_year - year_of_newest_extension)

def make_row(entry: DataDict) -> dict[str,str]:
    estimated_class_year = get_estimated_class_year(entry["extensions"])
    return {
        "callName": entry["callName"],
        "familyName": entry["familyName"],
        "classYear": estimated_class_year,
        "emails": entry["emails"],
    }

def write_emails(data: list[DataDict]) -> None:
    with open("content.csv", "w", encoding="utf-8") as output_file:
        writer = csv.DictWriter(
            output_file,
            fieldnames=["callName", "familyName", "classYear", "emails"],
            delimiter=";",
        )
        writer.writeheader()
        for entry in data:
            row = make_row(entry)
            if row["classYear"] == "_INFO" or row["classYear"] <= MAXIMUM_CLASS_YEAR:
                writer.writerow(row)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : list[DataDict] = json.load(data_file)
    write_emails(data)

if __name__ == "__main__":
    main()
