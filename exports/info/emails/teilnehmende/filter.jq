# Build data dict
[
  [
    .data.allEvents[].extensions |
    flatten |
    .[] |
    select(.participant.callName != "REDACTED") |
    select(.participant.familyName != "REDACTED")
  ] |
  group_by(.participant.callName + " " + .participant.familyName) |
  .[] |
  {
    "callName": .[0].participant.callName,
    "familyName": .[0].participant.familyName,
    "emails":
    (
      [.[].emailSelf] + ([.[].additionalEmails] | flatten) |
      unique |
      map(select(. != "REDACTED")) |
      map(select(. != "redacted")) |
      map(select(. != null)) |
      map(select(. != "")) |
      join(", ")
    ),
    "extensions": map(
      {
        "classYear": (.classYear // "_INFO" | sub("CLASS";"")),
        "eventYear": .event.year,
      }
    )
  } |
  select(.emails != "")
] |
sort_by(.callName, .familyName)
