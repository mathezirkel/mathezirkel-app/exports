.data |
{
  allEvents,
  "allParticipants":
    [
      .allParticipants[] |
      {
        gender,
        "extensions":
          [
            .extensions[] |
            select(.confirmed == true) |
            {
                eventId,
                "year": .event.year,
                "classYear": (.classYear // "" | sub("CLASS";"")),
            }
          ]
      }
    ],
}
