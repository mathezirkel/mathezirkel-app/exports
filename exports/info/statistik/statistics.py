import datetime
import json

EventDict = dict[str, int | str]
ExtensionDict = dict[str,str]
ParticipantDict = dict[str, str | list[ExtensionDict]]
DataDict = dict[str, list[EventDict] | list[ParticipantDict]]
StatisticsDict = dict[str,int]

def get_number_of_participants_year(year:int, participants: list[ParticipantDict]) -> int:
    return len([
        1
        for participant in participants
        if any(int(extension["year"]) == year for extension in participant["extensions"])
    ])

def get_statistics_year_by_event(
    year: int,
    participants: list[ParticipantDict],
    events: list[EventDict]
) -> StatisticsDict:
    return {
        event["name"]: len([
            1
            for participant in participants
            if event["id"] in [
                extension["eventId"]
                for extension in participant["extensions"]
            ]
        ])
        for event in events if event["year"] == year
    }

def get_statistics_year_by_class_year(year: int, participants: list[ParticipantDict]) -> StatisticsDict:
    return {
        class_year: len([
            1
            for participant in participants
            if str(class_year) in [
                extension["classYear"]
                for extension in participant["extensions"]
                if int(extension["year"]) == year
            ]
        ])
        for class_year in range(5,14)
    }

def get_statistics_year_by_gender(year: int, participants: list[ParticipantDict]) -> StatisticsDict:
    gender_counts = {
        "männlich": 0,
        "weiblich": 0,
        "nicht-binär": 0,
        "redacted": 0,
    }
    for participant in participants:
        if any(int(extension["year"]) == year for extension in participant["extensions"]):
            if (gender := participant["gender"].lower()) in ["männlich", "weiblich", "redacted"]:
                gender_counts[gender] += 1
            else:
                gender_counts["nicht-binär"] += 1
    return gender_counts

def get_statistics_year(year: int, data: DataDict) -> dict[str, int | StatisticsDict]:
    events: list[EventDict] = data["allEvents"]
    participants: list[ParticipantDict] = data["allParticipants"]
    return {
        "gesamt": get_number_of_participants_year(year, participants),
        "Veranstaltung": get_statistics_year_by_event(year, participants, events),
        "Klasse": get_statistics_year_by_class_year(year, participants),
        "Geschlecht": get_statistics_year_by_gender(year, participants),
    }

def get_statistics(data: DataDict) -> dict[int, dict[str, int | StatisticsDict]]:
    return {
        year: statistics
        for year in range(2014, datetime.datetime.now().year + 2)
        if (statistics := get_statistics_year(year,data))["gesamt"]
    }

def write_statistics(data: DataDict) -> None:
    with open("statistics.json", "w", encoding="utf-8") as output_file:
        json.dump(get_statistics(data), output_file, indent=4, ensure_ascii=False)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : DataDict = json.load(data_file)
    write_statistics(data)

if __name__ == "__main__":
    main()
