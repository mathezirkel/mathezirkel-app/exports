# Build data dict
[
  .data.event.instructorExtensions[] |
  {
    "Vorname": .instructor.callName,
    "Nachname": .instructor.familyName,
    "E-Mail": (.instructor.email // ""),
  }
] |

# Sort data
sort_by(.Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
