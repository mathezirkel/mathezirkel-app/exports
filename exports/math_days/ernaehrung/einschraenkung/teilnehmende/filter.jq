
# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  select(.foodRestriction != null and .foodRestriction != "") |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
    "Klasse": (.classYear | sub("CLASS";"")),
    "Telefon": (.telephone // ""),
    "Ernaehrung": (
      .nutrition |
      if . == "VEGAN" then "vegan"
      elif . == "VEGETARIAN" then "vegetarisch"
      elif . == "OMNIVORE" then "fleischhaltig"
      else .
      end
    ),
    "Ernaehrungseinschraenkung": .foodRestriction,
  }
] |

# Sort data
sort_by(.Ernaehrung, .Ernaehrungseinschraenkung, .Nachname, .Vorname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")

