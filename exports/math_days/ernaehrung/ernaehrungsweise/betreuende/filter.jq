# Build data dict
[
  .data.event.instructorExtensions[]
] |
group_by (.nutrition) |
map(
  {
    "key": (.[0].nutrition // "UNKNOWN"),
    "value": length,
  }
) |

# Build output
from_entries
