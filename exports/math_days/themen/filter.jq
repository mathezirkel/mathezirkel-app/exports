.data.event.zirkel |
map(
  {
    "name": (
      if (.name | startswith("Q"))
      then "Q"
      else .name
      end
    ),
    "topics": .topics,
  }
) |
map(select(.topics == [] | not)) |
group_by(.name) |
map(
  {
    "key": .[0].name,
    "value": map(.topics) | add | map(gsub("^\\s+|\\s+$";"")) | unique | sort,
  }
) |
from_entries
