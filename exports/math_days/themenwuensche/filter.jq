# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null)
] |

# Group topic wishes by class year
group_by (.classYear) |
map(
  {
    "key": (.[0].classYear | sub("CLASS";"")),
    "value": ([.[] | .topicWishes ] | add | join(", ")),
  }
) |

# Sort data
sort_by(.key) |

# Build output
from_entries
