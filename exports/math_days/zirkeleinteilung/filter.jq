{
  "mode": "fixed",
  "sortBy": "zirkel",
  "columnWidth": "4cm",
  "maxColumns": 4,
  "maxMembersPerCell": 12,
  "data":
    .data.event.zirkel |
    map(
      {
        "key": .name,
        "extra": (.room // "TBD"),
        "members": (
          .extensions |
          map(select(.confirmed == true and .timeOfSignoff == null)) |
          map(.participant) |
          sort_by(.callName, .familyName)
        ),
      }
    ) |
    map(select(.members != []))
}
