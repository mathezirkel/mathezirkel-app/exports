# Build data dict
[
  .data.event.instructorExtensions[] |
  select(.foodRestriction != null and .foodRestriction != "") |
  {
    "Vorname": .instructor.callName,
    "Nachname": .instructor.familyName,
    "Telefon": (.instructor.telephone // ""),
    "Ernaehrung": (
      .nutrition |
      if . == "VEGAN" then "vegan"
      elif . == "VEGETARIAN" then "vegetarisch"
      elif . == "OMNIVORE" then "fleischhaltig"
      else .
      end
    ),
    "Ernaehrungseinschraenkung": .foodRestriction,
  }
] |

# Sort data
sort_by(.Ernaehrung, .Ernaehrungseinschraenkung, .Nachname, .Vorname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
