
# Build data dict
[
  .data.event.instructorExtensions[] |
  select(.hasBirthday == true) |
  {
    "Vorname": .instructor.callName,
    "Nachname": .instructor.familyName,
    "Geburtsdatum": .instructor.birthDate,
  }
] |

# Sort data
sort_by((.Geburtsdatum | sub("^...."; "")), .Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")

