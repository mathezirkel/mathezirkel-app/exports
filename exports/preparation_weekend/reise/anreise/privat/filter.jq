# Build data dict
{
  "columns": {
    "blank_1": {
      "align": "c",
      "escape": false
    },
    "blank_2": {
      "align": "c",
      "escape": false
    },
    "Telefon": {
      "type": "telephone"
    },
    "Bemerkung": {
      "align": "X"
    }
  },
  "rows":
    [
      .data.event.instructorExtensions[] |
      select(.arrival == "PRIVATE") |
      {
        "blank_1": "$\\square$",
        "blank_2": "$\\square$",
        "Vorname": .instructor.callName,
        "Nachname": .instructor.familyName,
        "Telefon": (.instructor.telephone // ""),
        "Bemerkung": (.arrivalNotes // ""),
      }
    ] |
    sort_by(.Vorname, .Nachname),
}
