# Build data dict
{
  "mode": "hallway",
  "sortBy": "roomNumber",
  "columnWidth": "4cm",
  "maxColumns": 4,
  "data": (
    .data.event.instructorExtensions |
    group_by (.roomNumber) |
    map(
      {
        "key": (.[0].roomNumber // "TBD"),
        "members": (
          map(
            {
              callName: .instructor.callName,
              familyName: (.instructor.familyName[0:1] + "."),
            }
          ) |
          sort_by(.callName, .familyName)
        )
      }
    ) |
    sort_by(.roomNumber)
  )
}
