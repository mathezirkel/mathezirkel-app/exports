import json
import re

CellDict = dict[str, str | list[dict[str,str]]]

def sort(cells: list[CellDict], sort_by: str) -> list[CellDict]:
    class Sorter():
        @classmethod
        def _sort_key(cls, _cell: CellDict) -> None:
            return None

        @classmethod
        def sorted(cls, cells: list[CellDict]) -> list[CellDict]:
            return sorted(cells, key=cls._sort_key)

    class RoomSorter(Sorter):
        @classmethod
        def _sort_key(cls, cell: CellDict) -> tuple[int, int | str]:
            key = cell["key"]
            if key.isdigit():
                return (0, int(key))
            return (1, key)

    class ZirkelSorter(Sorter):
        _compiled_regex = re.compile(r"([5-9A-Z]|1[0-3])([5-9A-Z]|1[0-3])?([a-z]?)")

        @classmethod
        def _sort_key(cls, cell: CellDict) -> tuple[int, int | str]:
            key = cell["key"]
            match = cls._compiled_regex.match(key)
            if match:
                return tuple(
                    ((0, int(x)) if x.isdigit() else (1, x)) if x else (0,0)
                    for x in match.groups()
                )
            return ((1,str(key)), (0,0), (0,0))

    sorters = {
        "roomNumber": RoomSorter,
        "zirkel": ZirkelSorter
    }

    return sorters.get(sort_by, Sorter).sorted(cells)

def split_into_blocks(
    cells: list[CellDict],
    mode: str,
    max_columns: int,
    max_members_per_cell: int | None = None,
) -> list[list[CellDict]]:
    def partition_fixed(
        cells: list[CellDict],
        max_columns: int,
        max_members_per_cell: int | None = None,
    ) -> list[list[CellDict]]:
        partitioned_list = []
        current_partition = []
        current_weight_sum = 0
        for cell in cells:
            weight = get_cell_weight(cell, max_members_per_cell)
            if current_weight_sum + weight > max_columns:
                partitioned_list.append(current_partition)
                current_partition = []
                current_weight_sum = 0
            current_partition.append(cell)
            current_weight_sum += weight
        if current_partition:
            partitioned_list.append(current_partition)
        return partitioned_list

    def partition_hallway(
        cells: list[CellDict]
    ) -> list[list[CellDict]]:
        partitioned_list = []
        current_partition = []
        current_key = None
        for cell in cells:
            room_number = cell["key"]
            block_key = int(room_number) // 10 if room_number.isdigit() else "NON_DIGIT"
            if current_key is None:
                current_key = block_key
            if block_key != current_key and current_partition:
                partitioned_list.append(current_partition)
                current_partition = []
                current_key = block_key
            current_partition.append(cell)
        if current_partition:
            partitioned_list.append(current_partition)
        return partitioned_list

    if mode == "fixed":
        return partition_fixed(cells, max_columns, max_members_per_cell)
    if mode == "hallway":
        return partition_hallway(cells)
    raise ValueError(f"Unknown mode: {mode}")

def get_cell_weight(
    cell: CellDict,
    max_members_per_cell: int | None = None,
) -> int:
    if not max_members_per_cell:
        return 1
    return len(cell["members"]) // max_members_per_cell + 1

def get_members_splitted(cell: CellDict, max_members_per_cell: int | None = None) -> list[dict[str,str]]:
    members = cell['members']
    if not max_members_per_cell:
        return [members]
    cell_weight = get_cell_weight(cell, max_members_per_cell)
    k, r = divmod(len(members), cell_weight)
    splitted_members = []
    for i in range(cell_weight):
        splitted_members.append(members[i * k + min(i, r):(i + 1) * k + min(i + 1, r)])
    return splitted_members

def get_class_range(members: list[dict[str,str]]) -> str:
    min_class = min(int(member["classYear"]) for member in members)
    max_class = max(int(member["classYear"]) for member in members)
    if min_class == max_class:
        return f"Klasse {str(min_class)}"
    return f"Klasse {min_class} -- {max_class}"

def get_extra_information(cell: CellDict):
    if extra := cell.get("extra", None):
        return extra
    if "classYear" in cell["members"][0]:
        return get_class_range(cell["members"])
    return ""

def make_extra_row(row: list[CellDict], row_length: int, max_members_per_cell: int | None = None) -> str:
    if any(get_extra_information(cell) for cell in row):
        return " & ".join(
            rf"\multicolumn{{{get_cell_weight(cell, max_members_per_cell)}}}{{|l|}}{{\textit{{{get_extra_information(cell)}}}}}"
            for cell in row
        ) + rf"\\ \cline{{1-{row_length}}}"
    return ""

def make_member_list(members: list[dict[str,str]]):
    output = [
        r"\begin{itemize}[itemsep=0.1em,topsep=0pt,leftmargin=1.3em,before=\vspace{-0.5em},after=\vspace{-0.5em}]",
        r"\RaggedRight"
    ]
    output += [
        rf"\item {member['callName']} {member['familyName']}"
        for member in members
    ]
    output.append(r"\end{itemize}")
    return "\n".join(output)

def make_block(
    block: list[CellDict],
    max_columns: int,
    column_width: str,
    max_members_per_cell: int | None = None
) -> str:
    if not block:
        return ""
    alignment = "|" \
        + "|".join(
            [f"p{{{column_width}}}"] * min(
                sum(get_cell_weight(cell, max_members_per_cell) for cell in block),
                max_columns
            )
        ) \
        + "|"
    output = [rf"\begin{{tabular}}[t]{{{alignment}}}", r"\hline"]
    for row in split_into_blocks(block, "fixed", max_columns, max_members_per_cell):
        row_length = sum(get_cell_weight(cell, max_members_per_cell) for cell in row)
        # Table head
        output.append(
            " & ".join(
                rf"\multicolumn{{{get_cell_weight(cell, max_members_per_cell)}}}{{|c|}}{{\thead{{\Huge \textbf{{{cell['key']}}}}}}}"
                for cell in row
            ) + rf"\\ \cline{{1-{row_length}}}"
        )
        output.append(make_extra_row(row, row_length, max_members_per_cell))
        # Members
        output.append(
            "\n&\n".join(
                make_member_list(submembers)
                for cell in row
                for submembers in get_members_splitted(cell, max_members_per_cell)
            ) + rf"\\ \cline{{1-{row_length}}}"
        )

    output += [r"\end{tabular}", r"\vspace{0.75cm}"]
    return "\n".join(output) + "\n\n"

def make_assignment_list(
    cells: list[CellDict],
    mode: str,
    column_width: str,
    max_columns: int,
    max_members_per_cell: int | None = None,
) -> str:
    return "\n\n".join(
        make_block(block, max_columns, column_width, max_members_per_cell)
        for block in split_into_blocks(cells, mode, max_columns, max_members_per_cell)
    )

def write_assignment_list(data: dict[str, str | list[CellDict]]) -> None:
    with open("content.tex", "w", encoding="utf-8") as tex_file:
        mode = data["mode"]
        cells = sort(data["data"], data["sortBy"])
        column_width : str = data["columnWidth"]
        max_columns: int = data["maxColumns"]
        max_members_per_cell: int | None = data.get("maxMembersPerCell", None)
        tex_file.write(
            make_assignment_list(
                cells,
                mode,
                column_width,
                max_columns,
                max_members_per_cell,
            )
        )

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : dict[str, str | list[CellDict]] = json.load(data_file)
    write_assignment_list(data)

if __name__ == "__main__":
    main()
