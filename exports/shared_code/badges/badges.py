#!/usr/bin/env python3

import json

def write_metadata_commands(event_name: str, event_year: str) -> str:
    return (
        rf"\renewcommand{{\eventName}}{{{event_name}}}"
        rf"\renewcommand{{\eventYear}}{{{event_year}}}"
    )

def write_badge_participant(call_name: str):
    return rf"\badgeParticipant{{{call_name}}}"

def write_badge_instructor(call_name: str):
    return rf"\badgeInstructor{{{call_name}}}"

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data = json.load(data_file)
    event_name = data["name"]
    event_year = data["year"]
    participants = data.get("extensions", [])
    instructors = data.get("instructorExtensions", [])

    metadata_commands = write_metadata_commands(event_name, event_year)
    # There must not be an empty line between two badges.
    # Otherwise the layout will be scuffed.
    badges_participants = "\n".join(
        write_badge_participant(participant["participant"]["callName"])
        for participant in participants
    )
    badges_instructors = "\n".join(
        write_badge_instructor(instructor["instructor"]["callName"])
        for instructor in instructors
    )

    tex_string = "\n".join([metadata_commands, badges_participants, badges_instructors])

    with open('content.tex', 'w', encoding='utf-8') as tex_file:
        tex_file.write(tex_string)

if __name__ == "__main__":
    main()
