import datetime
import json
import re

InstructorDict = dict[str, dict[str,str]]
ParticipantDict = dict[str,str | dict[str,str]]
ZirkelDict = dict[str, str | list[str] | list[InstructorDict] | list[ParticipantDict]]
DataDict = dict[str, str | list[ZirkelDict]]

GERMAN_MONTHS = {
    1: "Januar",
    2: "Februar",
    3: "März",
    4: "April",
    5: "Mai",
    6: "Juni",
    7: "Juli",
    8: "August",
    9: "September",
    10: "Oktober",
    11: "November",
    12: "Dezember",
}

NUM_TO_WORDS = {
    1: "ein",
    2: "zwei",
    3: "drei",
    4: "vier",
    5: "fünf",
    6: "sechs",
    7: "sieben",
    8: "acht",
    9: "neun",
    10: "zehn",
    11: "elf",
    12: "zwölf",
}

def german_full_date(date: datetime.datetime) -> str:
    day = date.day
    month = GERMAN_MONTHS[date.month]
    year = date.year
    return f"{day:02d}. {month} {year}"

def make_certificate(
    participant: ParticipantDict,
    event_name: str,
    event_type: str,
    start_date: datetime.datetime,
    end_date: datetime.datetime,
    signature_date: datetime.datetime,
    default_signature : str,
    instructors: list[InstructorDict],
    zirkel_name: str,
    topics_list: list[str],
) -> str:
    call_name = participant["participant"]["callName"]
    family_name = participant["participant"]["familyName"]
    what = re.sub(r"\(.*?\)", "", event_name).strip()
    extra = "des"
    topics_header = "Inhalte"
    topics = ", ".join(topics_list)
    signature_date = german_full_date(signature_date)
    signature = default_signature
    subsignature = f"für das Team vom {what}"
    match event_type:
        case "ZIRKEL":
            when = f"im Schuljahr {start_date.year}/{end_date.year - 2000}"
            class_year = participant["classYear"]
            extra = f"der Klassenstufe {class_year} des"
            signing_location = "Augsburg"
            if instructors:
                signature = ", ".join(
                    f"{instructor['instructor']['callName']} {instructor['instructor']['familyName']}"
                    for instructor in instructors
                )
                subsignature = ""
        case "MATH_DAY":
            when = f"am {german_full_date(start_date)}"
            signing_location = "Augsburg"
        case "WINTER_CAMP":
            when = f"in den Winterferien {end_date.year}"
            event_duration_in_days = (end_date - start_date).days + 1
            what = f"{NUM_TO_WORDS[event_duration_in_days]}tägigen {what}"
            signing_location = "Violau"
        case "MATH_CAMP":
            when = f"in den Sommerferien {end_date.year}"
            event_duration_in_days = (end_date - start_date).days + 1
            what = f"{NUM_TO_WORDS[event_duration_in_days]}tägigen {what}"
            if zirkel_name == "Q":
                topics_header = "Angebotene Themen"
            signing_location = "Violau"

    return rf"\certificate{{{call_name}~{family_name}}}{{{when}}}{{{what}}}{{{extra}}}{{{topics_header}}}{{{topics}}}{{{signing_location}, den {signature_date}}}{{{signature}}}{{{subsignature}}}"

def write_certificates(data: DataDict) -> None:
    event_name: str = data["name"]
    event_type: str = data["type"]
    start_date = datetime.datetime.strptime(data["startDate"], "%Y-%m-%d")
    end_date = datetime.datetime.strptime(data["endDate"], "%Y-%m-%d")
    signature_date = datetime.datetime.strptime(data["certificateSignatureDate"], "%Y-%m-%d")
    default_signature: str = data["certificateDefaultSignature"] or ""

    tex_content_list = [
        make_certificate(
            participant,
            event_name,
            event_type,
            start_date,
            end_date,
            signature_date,
            default_signature,
            zirkel["instructorExtensions"],
            zirkel["name"],
            zirkel["topics"],
        )
        for zirkel in data["zirkel"]
        for participant in zirkel["extensions"]
    ]
    # Compile blank pdf if there is no certificate
    tex_content = "\n\\clearpage\n".join(tex_content_list) or r"\null"

    with open("content.tex", "w", encoding="utf-8") as tex_file:
        tex_file.write(tex_content)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : DataDict = json.load(data_file)
    write_certificates(data)

if __name__ == "__main__":
    main()
