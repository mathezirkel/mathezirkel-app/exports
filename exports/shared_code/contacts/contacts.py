import json
import vobject

DataDict = dict[str, str | list[str] | list[dict[str,str]]]

def create_card(entry: DataDict) -> vobject.vCard:
    card = vobject.vCard()

    # Name
    n = card.add("N")
    family_name = entry.get("familyName", "")
    call_name = entry.get("callName", "")
    n.value = vobject.vcard.Name(
        family=family_name,
        given=call_name,
    )

    # Formatted Name (required)
    fn = card.add("FN")
    formatted_name = f"{call_name} {family_name}"
    fn.value = formatted_name

    # Address
    if address := entry.get("address", {}):
        address_field = card.add("ADR")
        address_field.value = vobject.vcard.Address(
            street=f"{address.get('street', '')} {address.get('streetNumber', '')}",
            code=address.get("postalCode", ""),
            city=address.get("city", ""),
            country=address.get("country", ""),
        )

    # Org
    if class_year := entry.get("classYear", ""):
        org_field = card.add("ORG")
        org_field.value = [f"Klasse {class_year}"]

    # E-Mails
    if email := entry.get("emailSelf", ""):
        email_field = card.add("EMAIL")
        email_field.value = email
        email_field.type_param = "HOME"
    if email := entry.get("email", ""):
        email_field = card.add("EMAIL")
        email_field.value = email
        email_field.type_param = "HOME"
    for email in entry.get("additionalEmails", []):
        email_field = card.add("EMAIL")
        email_field.value = email
        email_field.type_param = "OTHER"

    # Telefon
    if telephone := entry.get("telephone", ""):
        telephone_field = card.add("TEL")
        telephone_field.value = telephone
        telephone_field.type_param = "HOME"
    for contact in entry.get("contacts", []):
        telephone_field = card.add("TEL")
        telephone_field.value = contact["telephone"]
        telephone_field.type_param = "X-NOTFALL"

    return card

def write_contact_file(data: list[DataDict]) -> None:
    with open("contact.vcf", "w", encoding="utf-8") as tex_file:
        tex_file.write("".join(create_card(entry).serialize() for entry in data))

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : list[DataDict] = json.load(data_file)
    write_contact_file(data)

if __name__ == "__main__":
    main()
