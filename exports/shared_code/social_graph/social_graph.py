import json
from pathlib import Path
import matplotlib.pyplot as plt
import networkx as nx

def get_social_graphs(data: list[dict[str, str | list[str]]]) -> list[nx.DiGraph]:
    """
    This function creates list of directed graphs representing the specified wishes during registration.

    Parameters:
        data: Data exported by the Mathezirkel App

    Returns:
        A list of directed graphes describing the specified wishes.
    """
    G = nx.DiGraph()

    # Create graph
    for participant in data:
        call_name = participant.get("callName", "")
        family_name = participant.get("familyName", "")
        participant_node = f"{call_name} {family_name}"
        class_year = participant.get("classYear", "")
        G.add_node(participant_node, class_year=class_year)

        for friend in participant.get("wishes", []):
            G.add_node(friend)
            G.add_edge(participant_node, friend)

    # Get weakly connected components of size at least 1
    wcc = [
        G.subgraph(component)
        for component in nx.weakly_connected_components(G) if len(component) > 1
    ]

    return wcc

def write_social_graphs(data: list[dict[str, str | list[str]]]):
    wcc = get_social_graphs(data)
    for i, component in enumerate(wcc):
        plt.figure(figsize=(40,20))
        pos = nx.spring_layout(component)
        nx.draw_networkx(
            component,
            pos,
            arrowstyle='->',
            arrowsize=50,
            node_size=500,
            with_labels=False,
            node_color="#d6faff"
        )
        labels = {
            node: f"{node}\n({attr.get('class_year', '?')})"
            for node, attr in component.nodes(data=True)
        }
        nx.draw_networkx_labels(component, pos, labels=labels, font_size=16, font_color='black')
        contained_class_years = [
            attr['class_year']
            for _, attr in component.nodes(data=True)
            if 'class_year' in attr
        ]
        for class_year in contained_class_years or ["other"]:
            class_dir = Path(f"./exports/{str(class_year)}")
            if not class_dir.exists():
                class_dir.mkdir(parents=True, exist_ok=True)
            plt.savefig(class_dir / f"wunsch_{i}.png")
        plt.clf()
        plt.close()

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : list[dict[str, str | list[str]]] = json.load(data_file)
    write_social_graphs(data)

if __name__ == "__main__":
    main()
