# Tabular helper

## Description

This Python script reads a JSON file (`data.json`) containing a list of objects and generates LaTeX code for an `xltabular` table.
The keys of the objects are used as table headers and the values as entries in the rows. For multicolumns, a list is used. The keys are assumed to be the same for any object.

Also, the number of rows is saved in the command `\rowcount`.

## Usage

1. **Input**:
    Create a JSON file named `data.json` in the same directory as the script.
    The file should contain a list of objects. Example:

    ```json
    {
        "rows": [
            {
                "name": "Alice",
                "age": 30,
                "telephone": ["03012345678", "03012345678"]
            },
            {
                "name": "Bob",
                "age": 25,
                "telephone": ["03012345678", "03012345678"]
            }
        ]
    }
    ```

    All rows must contain the same keys.

2. **Optional Configuration**:
    Optionally, add further keys for configuration. If configuration is not provided or keys are omitted, defaults are used. If the column name starts with `blank`, the column header will be empty.

    The key `columns` is an object containing configurations for each key in `data.json`.
    The optional key `arraystretch` defines the arraystretch of the table.

    Example:

    ```json
    {
        "columns": {
            "name": {
                "align": "r",
                "header_align": "c"
            },
            "age": {
                "align": "r",
                "escape": false,
                "header_align": "c"
            },
            "telphone": {
                "multi": true,
                "type": "telephone",
                "header_align": "l"
            }
        },
        "arraystretch": 1.0,
        "rows": [
            {
                "name": "Alice",
                "age": 30,
                "telephone": ["03012345678", "03012345678"]
            },
            {
                "name": "Bob",
                "age": 25,
                "telephone": ["03012345678", "03012345678"]
            }
        ]
    }
    ```

## Example output

```latex
\newcommand{\rowcount}{2}
\renewcommand{\arraystretch}{1.0}
\rowcolors{1}{gray!20}{white}
\begin{xltabular}[l]{\textwidth}{|r|r|l|l|}
\hline
\rowcolor{gray!40}
\textbf{name} & \textbf{age} & \multicolumn{2}{|l|}{\textbf{telephone}}\\
\hline
\endhead
Alice & 30 & \phonenumber[home-country=DE,foreign]{03012345678} & \phonenumber[home-country=DE,foreign]{03012345678}\\\hline
Bob & 25 & \phonenumber[home-country=DE,foreign]{03012345678} & \phonenumber[home-country=DE,foreign]{03012345678}\\\hline
\end{xltabular}
```

## Configuration options of columns

| Option | Description | Default |
|--------|-------------|---------|
| `align` | xltabular alignment of the column | `l` |
| `escape` | Whether the value should be LaTeX escaped | `true` |
| `multi` | Whether it is a multicolumn | `false0d975601-0e02-4a59-a036-3571b19fce2a` |
| `type` | Type of the column, possible values: `telephone`, `normal` | `normal` |
| `header_align` | Alignment of the header in multicolumn | `c` |
