from __future__ import annotations
import json
import re
from typing import TYPE_CHECKING
import more_itertools

if TYPE_CHECKING:
    from typing import Iterator

# Types
ColumnsSchemaDict = dict[str, dict[str,str]]
JSONType = bool | int | float | str | None
RowDict = dict[str, JSONType | list[JSONType]]
DataDict = dict[str, float | ColumnsSchemaDict | list[RowDict]]

def escape_latex(unsafe_string: str) -> str:
    replacements = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\textasciicircum{}',
        '\\': r'\textbackslash{}'
    }
    pattern = re.compile("|".join(re.escape(key) for key in replacements))

    def substitute(match: re.Match[str]) -> str:
        return replacements[match.group(0)]

    return pattern.sub(substitute, unsafe_string)

def get_max_items_of_multi_column(column_name: str, rows: list[RowDict]) -> int:
    return max((len(item[column_name]) for item in rows), default=0)

def update_schema_by_computed_values(data: DataDict) -> None:
    for column_name, column_schema in data.get("columns", {}).items():
        if column_schema.get("multi", False):
            column_schema["max_items"] = get_max_items_of_multi_column(column_name, data["rows"])

def get_arraystretch_command(arraystretch: int | float) -> str:
    return rf"\renewcommand{{\arraystretch}}{{{arraystretch}}}"

def get_alignment(item: DataDict, schema: ColumnsSchemaDict) -> Iterator[str]:
    def get_alignment_of_column(
        column_name: str,
        schema: ColumnsSchemaDict,
    ) -> str | list[str]:
        column_schema = schema.get(column_name, {})
        align = column_schema.get("align", "l")
        if column_schema.get("multi", False):
            max_items = column_schema.get("max_items")
            return [align]*max_items
        return align

    return more_itertools.collapse(
        get_alignment_of_column(column_name, schema)
        for column_name in item
    )

def get_alignment_of_multi_column(column_name: str, schema: ColumnsSchemaDict) -> str:
    return schema.get(column_name, {}).get("header_align", "c")

def get_header_row(item: DataDict, schema: ColumnsSchemaDict) -> str:
    def get_column_header(
        column_name: str,
        schema: ColumnsSchemaDict
    ) -> str:
        column_schema = schema.get(column_name, {})
        column_header = column_name if (not column_name.startswith("blank")) else ""
        if column_schema.get("multi", False):
            max_items = column_schema["max_items"]
            align = get_alignment_of_multi_column(column_name, schema)
            return rf"\multicolumn{{{max_items}}}{{|{align}|}}{{\textbf{{{column_header}}}}}"
        return rf"\textbf{{{column_header}}}"

    return [get_column_header(column_name, schema) for column_name in item]

def get_table_header(item: DataDict, schema: ColumnsSchemaDict) -> str:
    return "\n".join([
        r"\rowcolors{1}{gray!20}{white}",
        rf"\begin{{xltabular}}[l]{{\textwidth}}{{|{'|'.join(get_alignment(item, schema))}|}}",
        r"\hline",
        r"\rowcolor{gray!40}",
        rf"{' & '.join(get_header_row(item, schema))}\\",
        r"\hline",
        r"\endhead",
    ])

def get_table_footer() -> str:
    return r"\end{xltabular}"

def get_row_count_command(row_count: int) -> str:
    return rf"\newcommand{{\rowcount}}{{{row_count}}}"

def get_table_row(item: DataDict, schema: ColumnsSchemaDict) -> Iterator[str]:
    def get_value_representation(value: str, value_type: str, escape: bool = True):
        value = str(value)
        if escape:
            value = escape_latex(value)
        match value_type:
            case "telephone":
                if not value:
                    return ""
                return rf"\phonenumber[home-country=DE,foreign]{{{value}}}"
            case _:
                return value

    def get_table_cell(
        value: JSONType | list[JSONType],
        column_schema: dict[str,str]
    ) -> str | list[str]:
        value_type = column_schema.get("type", "normal")
        escape = column_schema.get("escape", True)
        if isinstance(value, list):
            max_items = column_schema["max_items"]
            return [
                get_table_cell(value[i], column_schema) if i < len(value) else ""
                for i in range(max_items)
            ]
        return get_value_representation(value, value_type, escape)

    return more_itertools.collapse(
        get_table_cell(value, schema.get(column_name, {}))
        for column_name, value in item.items()
    )

def write_table(data: DataDict) -> None:
    with open('content.tex', 'w', encoding='utf-8') as tex_file:
        if rows := data["rows"]:
            tex_file.write("\n".join([
                get_row_count_command(len(rows)),
                get_arraystretch_command(data.get("arraystretch", 1.0)),
                get_table_header(rows[0], data.get("columns", {})),
                *[
                    " & ".join(get_table_row(item, data.get("columns", {}))) + r"\\\hline"
                    for item in rows
                ],
                get_table_footer(),
            ]))
        else:
            tex_file.write(get_row_count_command(len(rows)))

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : list[DataDict] = json.load(data_file)
    update_schema_by_computed_values(data)
    write_table(data)

if __name__ == "__main__":
    main()
