import json
import re

ZirkelDict = dict[str, None | str]
InstructorExtensionDict = dict[str, str]
ZirkelPlanEntryDict = dict[str, None | bool | str | list[InstructorExtensionDict] | ZirkelDict]
ZirkelPlanSlotDict = dict[str, str | list[ZirkelPlanEntryDict]]
DataDict = dict[str, list[ZirkelPlanSlotDict] | list[ZirkelDict]]
GeometricZirkelPlanEntryDict = dict[str, bool | str | list[str]]
GeometricZirkelPlanSlotDict = list[GeometricZirkelPlanEntryDict]

class ZirkelSorter():
    _compiled_regex = re.compile(r"([5-9A-Z]|1[0-3])([5-9A-Z]|1[0-3])?([a-z]?)")

    @classmethod
    def _sort_key(cls, zirkel: ZirkelDict) -> tuple[int, int | str]:
        key: str = zirkel["name"]
        match = cls._compiled_regex.match(key)
        if match:
            return tuple(
                ((0, int(x)) if x.isdigit() else (1, x)) if x else (0,0)
                for x in match.groups()
            )
        return ((1,str(key)), (0,0), (0,0))

    @classmethod
    def sorted(cls, zirkels: list[ZirkelDict]) -> list[ZirkelDict]:
        return sorted(zirkels, key=cls._sort_key)

def make_entry(zirkel_plan_entry: ZirkelPlanEntryDict) -> GeometricZirkelPlanEntryDict:
    instructors = sorted(instructor.get("name", "") for instructor in zirkel_plan_entry.get("instructorExtensionsPublic", {}))
    color = (zirkel_plan_entry.get("color", "#ffffff") or "#ffffff")[1:]
    return {
        "instructors": instructors,
        "topic": (zirkel_plan_entry.get("topic", "") or "").strip(),
        "beamer": zirkel_plan_entry.get("beamer", False),
        "color": color,
        "render": True,
    }

def get_free_instructors(slot: ZirkelPlanSlotDict, instructors: list[InstructorExtensionDict]) -> list[str]:
    return [
        instructor["name"] for instructor in instructors
        if not instructor["id"] in [
            instructor["id"]
            for zirkel_plan_entry in slot["zirkelPlanEntries"]
            for instructor in zirkel_plan_entry["instructorExtensionsPublic"]
        ]
    ]

def make_row(
    slot: ZirkelPlanSlotDict,
    zirkels: list[ZirkelDict],
    instructors: list[InstructorExtensionDict],
    print_free_instructors: bool,
) -> GeometricZirkelPlanSlotDict:
    return [
        {
            "name": slot["name"],
            "render": True,
        }
    ] + [
        make_entry(
            next(
                (
                    entry for entry in slot["zirkelPlanEntries"]
                    if entry["zirkel"]["id"] == zirkel["id"]
                ),
                {}
            )
        )
        for zirkel in zirkels
    ] + (
        [
            {
                "freeInstructors": get_free_instructors(slot, instructors),
                "render": True,
            }
        ] if print_free_instructors else []
    )

def make_zirkelplan(
    zirkels: list[ZirkelDict],
    zirkel_plan_slots: list[ZirkelPlanSlotDict],
    instructors: list[InstructorExtensionDict],
    print_free_instructors: bool,
) -> list[GeometricZirkelPlanSlotDict]:
    return [
        make_row(slot, zirkels, instructors, print_free_instructors)
        for slot in zirkel_plan_slots
    ]

def get_column_alignment(
    zirkels: ZirkelDict,
    column_width: float,
    print_free_instructors: bool,
) -> list[str]:
    return [
        f"m{{{column_width}cm}}" # Slot
    ] + [
        f"t{{{column_width-0.5}cm}}@{{}}h{{0.5cm}}"
    ] * len(zirkels) + (
        [f"t{{{column_width}cm}}"] if print_free_instructors else [] # free
    )

def get_row_alignment(zirkel_plan_slots: list[ZirkelPlanSlotDict]) -> list[str]:
    return [
        [
            "t{1em}", # Header
            "t{1em}", # Rooms
        ]
    ] +  [
        ["t{2em}t{4em}"]*sum(slot["startTime"][:10] == date for slot in zirkel_plan_slots)
        for date in sorted(set(slot["startTime"][:10] for slot in zirkel_plan_slots))
    ]

def render_header_row(
    zirkels: list[ZirkelDict],
    print_free_instructors: bool,
) -> list[str]:
    return [
        r"\SetCell{c}\textbf{Zirkel}"
    ] + [
        rf"\SetCell[c=2]{{}}\textbf{{{zirkel['name']}}} &"
        for zirkel in zirkels
    ] + (
        [r"\textbf{frei}"] if print_free_instructors else []
    )

def render_room_row(
    zirkels: list[ZirkelDict],
    print_free_instructors: bool,
) -> list[str]:
    return [
        r"\SetCell{c}\textbf{Raum}"
    ] + [
        rf"\SetCell[c=2]{{}}\textbf{{{zirkel['room']}}} &"
        for zirkel in zirkels
    ] + (
        [r"\textbf{Teamzimmer}"] if print_free_instructors else []
    )

def render_cell(
    zirkel_plan_entry: GeometricZirkelPlanEntryDict,
    column_width: float,
    row_span: int,
    col_span: int,
    print_beamer: bool,
) -> tuple[str,str]:
    render = zirkel_plan_entry.get("render", False)
    if name := zirkel_plan_entry.get("name", None):
        return (rf"\SetCell[r=2]{{c}}\textbf{{{name if render else ''}}}", "")
    if free_instructors := zirkel_plan_entry.get("freeInstructors", None):
        free_instructors_string = ", ".join(free_instructors) if render and len(free_instructors) < 12 else ""
        return (rf"\SetCell[r=2]{{valign=t,halign=l}} {free_instructors_string}", "")
    instructors = ", ".join(zirkel_plan_entry["instructors"]) if render else ""
    topic: str = zirkel_plan_entry["topic"] if render else ""
    beamer = r"\clipbox{0pt}{\beamer}" if print_beamer and render and zirkel_plan_entry["beamer"] else ""
    color = zirkel_plan_entry["color"]
    return (
        rf"\SetCell[c={2*col_span-1}]{{valign=t,halign=l,bg={color}!50}} \clipbox{{0pt}}{{\parbox[t][1em][t]{{{column_width*col_span-0.5:.2f}cm}}{{{instructors}}}}} & \SetCell{{bg={color}!50}} {beamer}",
        rf"\SetCell[c={2*col_span},r={2*row_span-1}]{{valign=h,halign=l,bg={color}!50}} \clipbox{{0pt}}{{\parbox[t][{3*row_span:.2f}em][t]{{{column_width*col_span:.2f}cm}}{{{topic}}}}} &"
    )

def render_zirkel_plan(
    zirkelplan: list[GeometricZirkelPlanSlotDict],
    column_width: float,
    print_beamer: bool,
) -> list[tuple[list[tuple[str, str]], bool]]:
    comparison_keys = ["instructors", "topic"]
    rendered_slots = []
    for i, slot in enumerate(zirkelplan):
        disable_pagebreak = False
        rendered_zirkelplan_entries = []
        for j, entry in enumerate(slot):
            col_span = 1
            row_span = 1
            if zirkelplan[i][j]["render"]:
                while (
                    all(key in entry for key in comparison_keys)
                    and j + col_span < len(slot)
                    and (future_entry := zirkelplan[i][j+col_span])["render"]
                    and all(key in future_entry for key in comparison_keys)
                    and all(future_entry[key] == entry[key] for key in comparison_keys)
                ):
                    zirkelplan[i][j+col_span]["render"] = False
                    col_span += 1
                # Only take rowspan into account, if there is no colspan
                if row_span == 1:
                    while (
                        all(key in entry for key in comparison_keys)
                        and i + row_span < len(zirkelplan)
                        and (future_entry := zirkelplan[i+row_span][j])["render"]
                        and all(key in future_entry for key in comparison_keys)
                        and all(future_entry[key] == entry[key] for key in comparison_keys)
                    ):
                        zirkelplan[i+row_span][j]["render"] = False
                        row_span += 1
                        disable_pagebreak = True
            rendered_cell = render_cell(entry, column_width, row_span, col_span, print_beamer)
            rendered_zirkelplan_entries.append(rendered_cell)
        rendered_slots.append((rendered_zirkelplan_entries, disable_pagebreak))
    return rendered_slots

def make_tabular(
    header_row: list[ſtr],
    room_row: list[str],
    zirkelplan: list[GeometricZirkelPlanSlotDict],
    colspec: list[str],
    rowspec: list[str],
    column_width: float,
    print_beamer: bool,
) -> str:
    rendered_zirkelplan = render_zirkel_plan(
        zirkelplan,
        column_width,
        print_beamer,
    )
    return "\n".join([
        r"\begin{longtblr}{",
        f"colspec = {{|{'|'.join(colspec)}|}},",
        f"rowspec = {{|{'|[2pt]'.join('|'.join(subspec) for subspec in rowspec)}|}},",
        "}",
        "\n\\\\*\n".join([
            "\n&\n".join(row)
            for row in [header_row, room_row]
        ]),
        r"\\",
        "\n".join(
            "\n\\\\*\n".join(
                "\n&\n".join(subrow)
                for subrow in [[tup[i] for tup in row] for i in range(2)]
            )
            + "\n\\\\"
            + ("*" if disable_pagebreak else "")
            for row, disable_pagebreak in rendered_zirkelplan
        ),
        r"\end{longtblr}",
    ])

def make_tabulars(
    zirkels: list[ZirkelDict],
    zirkel_plan_slots: list[ZirkelPlanSlotDict],
    instructors: list[InstructorExtensionDict],
    column_width: float,
    number_of_columns_per_page: int,
    print_beamer: bool,
    print_free_instructors: bool,
) -> list[str]:
    header_row = render_header_row(zirkels, print_free_instructors)
    room_row = render_room_row(zirkels, print_free_instructors)
    colspec = get_column_alignment(zirkels, column_width, print_free_instructors)
    rowspec = get_row_alignment(zirkel_plan_slots)
    zirkelplan = make_zirkelplan(
        zirkels,
        zirkel_plan_slots,
        instructors,
        print_free_instructors,
    )
    return [
        make_tabular(
            header_row[i:i+number_of_columns_per_page],
            room_row[i:i+number_of_columns_per_page],
            [row[i:i+number_of_columns_per_page] for row in zirkelplan],
            colspec[i:i+number_of_columns_per_page],
            rowspec,
            column_width,
            print_beamer,
        )
        for i in range(0, len(zirkelplan[0]), number_of_columns_per_page)
    ]

def write_zirkelplan(data: DataDict) -> None:
    zirkels: list[ZirkelDict] = ZirkelSorter.sorted(data["zirkel"])
    zirkel_plan_slots: list[ZirkelPlanSlotDict] = sorted(data["zirkelPlanSlots"], key=lambda x: x["startTime"])
    instructors: list[InstructorExtensionDict] = sorted(data["instructorExtensionsPublic"], key=lambda x: x["name"])
    column_width: float = data["columnWidth"]
    number_of_columns_per_page: int = data["numberOfColumnsPerPage"]
    print_beamer: bool = data["printBeamer"]
    print_free_instructors: bool = data["printFreeInstructors"]
    colors = ["#ffffff"] + [
        entry["color"]
        for slot in zirkel_plan_slots
        for entry in slot["zirkelPlanEntries"]
    ]
    colors_tex = "\n".join(
        rf"\definecolor{{{color[1:]}}}{{HTML}}{{{color[1:]}}}"
        for color in colors
    )
    zirkelplan_tabulars = "\n\\clearpage\n".join(
        make_tabulars(zirkels, zirkel_plan_slots, instructors, column_width, number_of_columns_per_page, print_beamer, print_free_instructors)
    )
    with open("content.tex", "w", encoding="utf-8") as tex_file:
        tex_file.write(colors_tex)
        tex_file.write("\n")
        tex_file.write(zirkelplan_tabulars)

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : DataDict = json.load(data_file)
    write_zirkelplan(data)

if __name__ == "__main__":
    main()
