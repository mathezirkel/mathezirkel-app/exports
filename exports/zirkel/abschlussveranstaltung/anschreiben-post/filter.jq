# Build data dict
[
  .data.event |
  .extensions[] |
  select(.certificate == true and .participates == false) |
  {
    "callName": .participant.callName,
    "familyName": .participant.familyName,
    "street": .participant.street,
    "streetNumber": .participant.streetNumber,
    "postalCode": .participant.postalCode,
    "city": .participant.city,
  }
] |
sort_by(.callName, .familyName) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
