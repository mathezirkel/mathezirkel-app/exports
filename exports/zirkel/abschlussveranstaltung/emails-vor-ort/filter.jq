# Build data dict
[
  .data.event.extensions[] |
  select(.participates == true) |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
    "E-Mail Schueler:in": (.emailSelf // ""),
    "Weitere E-Mails": (.additionalEmails | join(", ")),
  }
] |

# Sort data
sort_by(.Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
