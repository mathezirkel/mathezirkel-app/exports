# Build data dict
.data.event |
{
  name,
  "zirkel": (
    .zirkel |
    map(
      {
        name,
        "extensions": (
          .extensions |
          map(
            select(.certificate) |
            select(.participates) |
            .participant
          ) |
          sort_by(.callName, .familyName)
        ),
        "instructorExtensions": (
          .instructorExtensions |
          map(.instructor.callName)
        ) |
        sort,
      }
    )
  )
}

