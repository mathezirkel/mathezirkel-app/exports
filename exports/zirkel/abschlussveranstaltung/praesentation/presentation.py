import json
import re

ZirkelDict = dict[str, str | list[str] | list[dict[str,str]]]
DataDict = dict[str, str | list[ZirkelDict]]

class ZirkelSorter():
    _compiled_regex = re.compile(r"([5-9A-Z]|1[0-3])([5-9A-Z]|1[0-3])?([a-z]?)")

    @classmethod
    def _sort_key(cls, zirkel: ZirkelDict) -> tuple[int, int | str]:
        key: str = zirkel["name"]
        match = cls._compiled_regex.match(key)
        if match:
            return tuple(
                ((0, int(x)) if x.isdigit() else (1, x)) if x else (0,0)
                for x in match.groups()
            )
        return ((1,str(key)), (0,0), (0,0))

    @classmethod
    def sorted(cls, zirkels: list[ZirkelDict]) -> list[ZirkelDict]:
        return sorted(zirkels, key=cls._sort_key)

    @classmethod
    def get_class_range(cls, zirkel: ZirkelDict) -> list[int]:
        key: str = zirkel["name"]
        match = cls._compiled_regex.match(key)
        if match:
            return sorted({
                y
                for x in match.groups()[:2]
                for y in ([int(x)] if x and x.isdigit() else [11,12,13] if x == "Q" else [])
            })
        return []

def make_participants_tabular(participants: list[dict[str,str]]) -> str:
    return "\n".join([
        r"\begin{tabular}{c}",
        "\n".join(
            rf"{{{participant['callName']}}}~{{{participant['familyName']}}}\\"
            for participant in participants
        ),
        r"\end{tabular}",
    ])

def make_certificate_frame(name: str, zirkel: ZirkelDict) -> str:
    class_range = ", ".join(str(c) for c in ZirkelSorter.get_class_range(zirkel))
    instructors = ", ".join(zirkel["instructorExtensions"])
    participants = zirkel["extensions"]
    return rf"\certificateframe{{{name}}}{{{class_range}}}{{{instructors}}}{{{make_participants_tabular(participants)}}}"

def write_presentation(data: DataDict) -> None:
    name: str = data["name"]
    zirkels: list[ZirkelDict] = ZirkelSorter.sorted(data["zirkel"])
    with open("content.tex", "w", encoding="utf-8") as tex_file:
        tex_file.write(
            "\n".join(
                make_certificate_frame(name, zirkel) for zirkel in zirkels
            )
        )

def main() -> None:
    with open("data.json", "r", encoding="utf-8") as data_file:
        data : DataDict = json.load(data_file)
    write_presentation(data)

if __name__ == "__main__":
    main()
