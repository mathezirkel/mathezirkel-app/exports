# Build data dict
[
  .data.event.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
    "Klasse": (.classYear | sub("CLASS";"")),
    "E-Mail Schueler:in": (.emailSelf // ""),
    "Weitere E-Mails": (.additionalEmails | join(", ")),
  }
] |

# Sort data
sort_by(.Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
