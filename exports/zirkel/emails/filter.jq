[
  .data.zirkel.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  {
    emailSelf: .emailSelf,
    additionalEmails: .additionalEmails
  }
] |
[.[] | flatten] |
flatten |
map(select(. != null)) |
map(select(. != "")) |
unique |
join(", ")
