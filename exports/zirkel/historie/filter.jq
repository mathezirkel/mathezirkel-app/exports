[
  .data.zirkel.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  .participant |
  {
    "Vorname": .callName,
    "Nachname": .familyName,
    "Historie": (
      [
        .extensions[] |
        {
          "Veranstaltung": .event.name,
          "Jahr": .event.year,
          "Zirkel": [ .zirkel[].name ],
          "startDate": .event.startDate,
        }
      ] |
      sort_by(.startDate, .Veranstaltung) |
      del(.[].startDate)
    )
  }
]