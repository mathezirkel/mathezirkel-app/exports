# Compute maximal amount of contacts
(
  [
    .data.zirkel.extensions[] |
    select(.confirmed == true and .timeOfSignoff == null) |
    .contacts |
    length
  ] |
  max
) as $maxContacts |

# Build data dict
[
  .data.zirkel.extensions[] |
  select(.confirmed == true and .timeOfSignoff == null) |
  {
    "Vorname": .participant.callName,
    "Nachname": .participant.familyName,
    "Geschlecht": .participant.gender,
    "Klasse": (.classYear | sub("CLASS";"")),
    "Themenwünsche": (.topicWishes | join(", ")),
    "Notizen": (.notes // ""),
    "Geburtsdatum": .participant.birthDate,
    "Telefon (für Rückfragen)": (.telephone // ""),
  }
  +
  (
    .contacts as $contacts |
    reduce range(0; $maxContacts) as $i
    ({};
      .["Notfallname \($i+1)"] = ($contacts[$i].name // "") |
      .["Notfallnummer \($i+1)"] = ($contacts[$i].telephone // "")
    )
  )
  +
  {

    "E-Mail Schueler:in": (.emailSelf // ""),
    "Weitere E-Mails": (.additionalEmails | join(", "))
  }
] |

# Sort data
sort_by(.Vorname, .Nachname) |

# Build csv
(.[0]? // {} | keys_unsorted), .[] |
join(";")
